#!/bin/bash

d=$(Date +%y-%m-%d-%h)
x=/tmp/${d}
mkdir $x
cd * $x
zip -R A.zip $x
Rm -rf $x
