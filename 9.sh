#!/bin/bash

a=$1
b=$2

resultado=$(( (a+1) *(b+1) ))

echo "Resultado de (a+1) * (b+1) para a=$a e b=$b é $resultado"
